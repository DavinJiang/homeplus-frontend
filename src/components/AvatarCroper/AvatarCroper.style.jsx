import styled from 'styled-components';

export const ContainerCroper = styled.div`
  height: 100%;
  .croper {
    position: relative;
    height: 68vh;
    top: 6vh;
    left: 50%;
    transform: translateX(-50%);
    .reactEasyCrop_Container {
      height: 64vh;
    }
  }
  .control {
    position: relative;
    background-color: #fff;
    // max-height: 26vh;
    height: 100%;
    z-index: 999;
  }

  .slider {
    display: flex;
    align-items: center;
    height: 5%;
    position: absolute;
    top: 5%;
    width: 80%;
    left: 50%;
    transform: translateX(-50%);
    .zoomLabel {
      margin-left: 15px;
    }
  }

  .buttons {
    height: 5%;
    position: absolute;
    top: 13%;
    left: 50%;
    transform: translateX(-50%);
  }
`;
