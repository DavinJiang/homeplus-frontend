import { React } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import TextField from '@mui/material/TextField';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import { updateField } from '../../../../../store/reducers/form/form.actions';
import { TextFieldGroup } from '../../../styles/TextFieldGroup.style';

const LocationFields = ({ values, updateFields, first2CateErrors }) => {
  const stateItems = ['NSW', 'VIC', 'QLD', 'WA', 'SA', 'TAS', 'ACT', 'NT'];
  const handleChange = (type) => (e) => {
    updateFields(type, e.target.value);
  };

  return (
    <>
      <h4>Where is the location?</h4>
      <TextFieldGroup>
        <TextField
          required
          id="street"
          label="Street"
          variant="outlined"
          name="title"
          value={values.street}
          onChange={handleChange('street')}
          sx={{ width: 220 }}
          error={first2CateErrors && values.street.length < 5}
          helperText={first2CateErrors && values.street.length < 5 ? 'At least 5 characters' : ' '}
        />
        <TextField
          required
          id="suburb"
          label="Suburb"
          variant="outlined"
          name="suburb"
          value={values.suburb}
          onChange={handleChange('suburb')}
          sx={{ width: 130 }}
          error={first2CateErrors && values.suburb === ''}
          helperText={first2CateErrors && values.suburb === '' ? 'Incorrect' : ' '}
        />
        <FormControl sx={{ width: 100 }} className="selector">
          <InputLabel id="state-select-label">State</InputLabel>
          <Select
            labelId="state-select-label"
            name="state"
            value={values.state}
            label="State"
            onChange={handleChange('state')}
          >
            {stateItems.map((item, index) => (
              <MenuItem value={item} key={index}>
                {item}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        <TextField
          required
          id="postcode"
          label="Postcode"
          variant="outlined"
          name="postcode"
          value={values.postcode}
          onChange={handleChange('postcode')}
          sx={{ width: 120 }}
          type="number"
          error={first2CateErrors && values.postcode.length !== 4}
          helperText={first2CateErrors && values.postcode.length !== 4 ? 'Incorrect' : ' '}
        />
      </TextFieldGroup>
    </>
  );
};

LocationFields.propTypes = {
  values: PropTypes.object.isRequired,
  updateFields: PropTypes.func,
  first2CateErrors: PropTypes.bool,
};

const mapDispatchToProps = (dispatch) => ({
  updateFields: (name, value) => dispatch(updateField(name, value)),
});

const mapStateToProps = ({ taskForm }) => ({
  values: taskForm,
});

export default connect(mapStateToProps, mapDispatchToProps)(LocationFields);
