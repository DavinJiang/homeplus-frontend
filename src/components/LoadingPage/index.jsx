import React from 'react';
import CircularProgress from '@mui/material/CircularProgress';
import Box from '@mui/material/Box';
import { Loading } from './LoadingPage.style';

const LoadingPage = () => (
  <Loading>
    <Box className="loading">
      <CircularProgress />
    </Box>
  </Loading>
);

export default LoadingPage;
