import styled from 'styled-components';
import { createGlobalStyle } from 'styled-components';

export const MenuLink = styled.a`
  padding: 1rem 2rem;
  cursor: pointer;
  text-align: center;
  text-decoration: none;
  color: #67bc98;
  transition: all 0.3s ease-in;
  font-size: 0.9rem;
`;

export const MenuLinkWithBackground = styled.a`
  padding: 0.8rem 1.6rem;
  cursor: pointer;
  border-radius: 25px;
  text-align: center;
  text-decoration: none;
  color: White;
  transition: all 0.3s ease-in;
  font-size: 0.9rem;
  background-color: #52ab98;
`;

export const Nav = styled.div`
  position: fixed;
  top: 0;
  width: 100%;
  max-height: 28vh;
  min-height: 6vh;
  display: flex;
  // border-bottom: 2px ridge #fcfaf9;
  align-items: center;
  flex-wrap: wrap;
  background: white;
  box-shadow: 0 5px 10px rgba(0, 0, 0, 0.05), 0 6px 6px rgba(0, 0, 0, 0.07);
  z-index: 99999;
  padding: 0;
  left: 0;
  right: 0;
  a {
    text-decoration: none;
  }
  > div {
    display: flex;
    max-width: 1120px;
    position: relative;
    left: 50%;
    transform: translateX(-50%);
    margin: 0;
    @media (max-width: 768px) {
      justify-content: space-between;
      align-items: center;
      width: 100vw;
      padding-left: 3%;
    }
  }
`;

export const Logo = styled.img`
  margin: 5px;
  margin-top: 10px;
  max-width: 55px;
  height: auto;
  &:hover {
    cursor: pointer;
  }
`;

export const MenuLinkAvatar = styled.img`
  margin: 10px;
  max-width: 55px;
  height: auto;
  width: 40px;
  height: 40px;
  object-fit: cover;
  border-radius: 50%;
`;

export const Menu = styled.div`
  width: 80vw;
  display: flex;
  justify-content: space-between;
  padding-left: 2rem;
  align-items: center;
  position: relative;
  .user {
    min-width: 100px;
    display: flex;
    align-items: center;
    > p {
      padding-left: 15px;
      display: none;
    }
  }
  > div {
    display: flex;
  }
  @media (max-width: 768px) {
    background-color: white;
    position: absolute;
    border-radius: 5px;
    right: 10px;
    top: 70px;
    width: 150px;
    padding-left: 0;
    padding-bottom: 5px;
    overflow: hidden;
    flex-direction: column;
    max-height: ${({ isOpen }) => (isOpen ? '300px' : '0')};
    display: ${({ isOpen }) => (isOpen ? 'inline' : 'none')};
    transition: max-height 0.3s ease-in;
    > div {
      width: 100%;
      padding-left: 0;
      flex-direction: column;
      align-items: center;
      > div {
        width: 100%;
      }
      button {
        margin-right: 0;
      }
    }
    .user {
      > p {
        display: inline;
      }
    }
  }
`;

export const Hamburger = styled.div`
  display: none;
  flex-direction: column;
  cursor: pointer;
  width: 25px;

  .burger {
    height: 2px;
    width: 25px;
    background: #444;
    margin-bottom: 4px;
    border-radius: 5px;
    transition: all 0.5s ease-in-out;

    &:before,
    &:after {
      content: '';
      position: absolute;
      height: 2px;
      width: 25px;
      background: #444;
      margin-bottom: 4px;
      border-radius: 5px;
      transition: all 0.5s ease-in-out;
    }
    &:before {
      transform: translateY(-6px);
    }
    &:after {
      transform: translateY(6px);
    }
  }

  .isopen {
    transform: translateX(-45px);
    background: transparent;
    &:before {
      transform: rotate(45deg) translate(35px, -35px);
    }
    &:after {
      transform: rotate(-45deg) translate(35px, 35px);
    }
  }

  @media (max-width: 768px) {
    display: flex;
    position: absolute;
    right: 25px;
  }
`;

export const ButtonsSet = styled.div`
  display: flex;
  @media (max-width: 768px) {
    flex-direction: column;
  }
`;

export const Container = styled.div`
  position: absolute;
  justify-content: center;
  align-items: center;
  align-above: 1px;
  z-index: 1000;
`;

export const GlobalStyle = createGlobalStyle`
* {
  box-sizing: border-box;
  margin:0;
  padding: 0;
  font-family: 'Arial', sans-serif;
}
`;
