import React, { useMemo } from 'react';
import PropTypes from 'prop-types';
import Box from '@mui/material/Box';
import Rating from '@mui/material/Rating';
import Typography from '@mui/material/Typography';

const Rates = ({ rating }) => {
  const sumOfRating = useMemo(() => {
    if (rating.sumOfRating) return rating.sumOfRating;
    return 0;
  }, [rating]);
  const numOfReviews = useMemo(() => {
    if (rating.numOfReviews) return rating.numOfReviews;
    return 0;
  }, [rating]);
  return (
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'column',
        alignSelf: 'flex-end',
        width: '250px',
        height: '100px',
        margin: '10px 0 0',
        '@media (max-width: 445px)': {
          margin: '30px 0 0',
          alignSelf: 'flex-start',
        },
      }}
    >
      {rating ? (
        <>
          <Rating name="read-only" value={rating.sumOfRating} precision={0.5} readOnly />
          <Typography
            component="legend"
            sx={{ paddingTop: 2 }}
          >{`${sumOfRating} stars from ${numOfReviews} reviews`}</Typography>
        </>
      ) : (
        <p>No reviews yet</p>
      )}
    </Box>
  );
};

Rates.propTypes = {
  rating: PropTypes.object.isRequired,
};

export default Rates;
