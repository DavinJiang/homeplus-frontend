import React from 'react';
import PropTypes from 'prop-types';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';

const Introduction = ({ tasker }) => {
  return (
    <Box sx={{ display: 'flex', flexDirection: 'column', margin: '50px', marginTop: 0 }}>
      <Typography variant="h6" gutterBottom component="div" sx={{ fontWeight: 500 }}>
        Skill Description
      </Typography>
      <Typography>{tasker.skills_description}</Typography>
      <Typography variant="h6" gutterBottom component="div" sx={{ fontWeight: 500, marginTop: '40px' }}>
        Certifications
      </Typography>
      <Typography>{tasker.certifications}</Typography>
    </Box>
  );
};

Introduction.propTypes = {
  tasker: PropTypes.object.isRequired,
};

export default Introduction;
