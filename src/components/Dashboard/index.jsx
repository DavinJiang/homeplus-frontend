import React, { useEffect, useState, useMemo } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import _ from 'lodash';
import Box from '@mui/material/Box';
import DashboardMenu from '../DashboardItems/DashboardMenu';
import DashboardTitleLine from '../DashboardItems/DashboardTitleLine';
import DashboardTaskerForm from '../DashboardItems/DashboardTaskerForm';
import DashboardPostedTasks from '../DashboardItems/DashboardPostedTasks';
import DashboardUserProfile from '../DashboardItems/DashboardUserProfile';
import DashboardPayment from '../DashboardItems/DashboardPayment';
import DashboardTaskerProfile from '../DashboardItems/DashboardTaskerProfile';
import DashboardHome from '../DashboardItems/DashboardHome';
import DashboardTasker from '../DashboardItems/DashboardTasker';
import TaskerDiasbleInfo from '../DashboardItems/DashboadDisableInfo/TaskerDiasbleInfo';
import { CustomBox, DashboardBox } from './Dashboard.style';

const Dashboard = ({ tasksOfUser, tasksOfTasker }) => {
  const [title, setTitle] = useState('Home');
  const { user, tasker } = useSelector((state) => state.currentUser);
  const [isTasker, setIsTasker] = useState(false);
  const [isEditting, setIsEditting] = useState(false);
  const TaskerEditableItems = useMemo(() => ['Payment', 'Profile'], []);
  const userEditableItems = useMemo(() => ['User Profile'], []);
  const turnOnOffItem = useMemo(() => ['Tasker Dashboard'], []);
  const taskerItems = useMemo(() => ['Tasks', 'Payment', 'Profile'], []);

  const onMenuClick = (menuItem) => {
    setTitle(menuItem);
  };

  useEffect(() => {
    if (user.is_tasker !== isTasker) {
      setIsTasker(user.is_tasker);
    }
  }, [user, tasker, isTasker]);

  const dashboardTitle = useMemo(() => {
    if (userEditableItems.includes(title)) {
      return <DashboardTitleLine title={title} edit setIsEditting={setIsEditting} />;
    } else if (TaskerEditableItems.includes(title)) {
      return <DashboardTitleLine title={title} edit disable={!isTasker} />;
    } else if (turnOnOffItem.includes(title)) {
      return <DashboardTitleLine title={title} taskMode />;
    } else if (title) {
      return <DashboardTitleLine title={title} />;
    }
  }, [TaskerEditableItems, userEditableItems, title, turnOnOffItem, isTasker]);

  const TaskerDashboardStatus = useMemo(() => {
    if (!_.isEmpty(user)) {
      if (!user.is_tasker) {
        return <TaskerDiasbleInfo message="Not a tasker yet!" />;
      } else {
        setIsTasker(true);
        if (!user.is_tasker_data) {
          return <DashboardTaskerForm />;
        } else {
          return <DashboardTasker tasksOfTasker={tasksOfTasker} />;
        }
      }
    }
  }, [user, tasksOfTasker]);

  const taskerItemsPresent = useMemo(() => {
    if (isTasker && user.is_tasker_data) {
      switch (title) {
        case 'Tasks':
          return <DashboardPostedTasks tasksOfTasker={tasksOfTasker} />;
        case 'Payment':
          return <DashboardPayment />;
        case 'Profile':
          return <DashboardTaskerProfile />;
        default:
          return;
      }
    } else {
      if (taskerItems.includes(title)) {
        if (!isTasker) {
          return <TaskerDiasbleInfo message="Not a tasker yet!" />;
        }
        if (!user.is_tasker_data) {
          return <TaskerDiasbleInfo message="No tasker data yet!" />;
        }
      } else {
        return;
      }
    }
  }, [isTasker, taskerItems, title, tasksOfTasker, user]);

  return (
    <DashboardBox>
      <DashboardMenu onMenuClick={onMenuClick} />
      <CustomBox>
        {dashboardTitle}
        <Box sx={{ width: '100%', display: 'flex', justifyContent: 'center', paddingTop: '1.5rem' }}>
          {title === 'Home' && <DashboardHome tasksOfUser={{ ...tasksOfUser, ...tasksOfTasker }} />}
          {title === 'Posted Tasks' && <DashboardPostedTasks tasksOfUser={tasksOfUser} />}
          {title === 'Tasker Dashboard' && TaskerDashboardStatus}
          {title === 'User Profile' && <DashboardUserProfile isEditting={isEditting} setIsEditting={setIsEditting} />}
          {taskerItemsPresent}
        </Box>
      </CustomBox>
    </DashboardBox>
  );
};

Dashboard.propTypes = {
  tasksOfUser: PropTypes.any,
  tasksOfTasker: PropTypes.any,
};

export default Dashboard;
