import styled from 'styled-components';

export const Wrapper = styled.div`
  width: 80%;
  display: flex;
  padding: 3%;
  border-radius: 10px;
  background-color: #fff;
  box-shadow: 0 10px 20px rgba(0, 0, 0, 0.23), 0 6px 6px rgba(0, 0, 0, 0.25);
  @media (max-width: 768px) {
    width: 80%;
    padding: 5%;
    justify-content: center;
  }
  .infoBox {
    width: 100%;
  }
`;

export const Left = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  @media (max-width: 768px) {
    flex-direction: column;
  }
`;

export const PicArea = styled.div`
  height: 50%;
  width: 50%;
  align-items: center;
  flex-direction: column;
  align-items: center;
  display: flex;
  @media (max-width: 768px) {
    padding-top: 15px;
    width: 100%;
  }
`;

export const Pic = styled.div`
  position: relative;
  width: 150px;
  height: 150px;
  border-radius: 50%;
  left: 50%;
  transform: translateX(-50%);
  background-color: #444;
`;

export const Username = styled.div`
  height: 40px;
  text-align: center;
  margin-top: 1rem;
  font-size: 20px;
`;

export const GenderArea = styled.div`
  margin-top: 2rem;
  padding-left: 4rem;
`;

export const LineWrapper = styled.div`
  height: 40px;
  padding-top: 10px;
  display: flex;
  // justify-content: space-between;
  @media (max-width: 768px) {
    padding: 5%;
    flex-direction: column;
  }
`;

export const Labal = styled.div`
  width: 120px;
  text-align: left;
  font-weight: 500;
  font-size: 18px;
`;

export const Value = styled.div`
  font-size: 16px;

  word-wrap: break-word;
  @media (max-width: 768px) {
    padding: 3%;
    padding-left: 5%;
  }
`;

export const Right = styled.div`
  width: 70%;
  height: 100%;
`;

export const TextArea = styled.div`
  height: 120px;
  width: 100%;
`;
