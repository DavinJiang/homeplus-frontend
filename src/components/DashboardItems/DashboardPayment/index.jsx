import React from 'react';
import { useSelector } from 'react-redux';
import { Box } from '@mui/system';
import Typography from '@mui/material/Typography';

const Payment = () => {
  const { tasker } = useSelector(state => state.currentUser);
  return (
    <div>
      <Box
        sx={{
          ml: '20px',
          display: 'flex',
          justifyContent: 'left',
          color: '#444',
        }}
      >
        <dl>
          <dt>
            <Typography variant="h6" sx={{ fontWeight: '200' }}>
              Bank Account
            </Typography>
          </dt>
          <br />
          <dd>
            <Typography sx={{ fontSize: '20' }}>
              {`BSB Number: ${tasker.bank_bsb}`}
            </Typography><br />
            <Typography sx={{ fontSize: '20' }}>
              {`Account Number: ${tasker.bank_account}`}
            </Typography>
          </dd>
        </dl>
      </Box>
    </div>
  );
};

export default Payment;
