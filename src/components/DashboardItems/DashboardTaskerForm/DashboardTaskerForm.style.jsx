import styled from 'styled-components';

export const TaskerForm = styled.form`
  .selectionRow {
    p {
      font-size: 20px;
      margin-bottom: 5px;
    }
    flex-direction: column;
    width: 95%;
    @media (max-width: 768px) {
      .MuiTextField-root {
        width: 350px;
      }

      .MuiInputBase-input,
      .MuiInputBase-root.MuiOutlinedInput-root {
        width: 350px;
      }
    }
  }

  .submit {
    width: 92%;
    display: flex;
    alignitems: center;
    justifycontent: end;
    margin: 1rem;
  }
  //   .MuiTextField-root {
  //     width: 80%;
  //   }
`;
