import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import Box from '@mui/material/Box';
import CustomButton from '../../CustomButton';
import TaskerInput from './TaskerInput';
import PaymentInput from './PaymentInput';
import CategorySelection from './CategorySelection';
import { initValues } from './initValues';
import SuccessErrorModal from '../../SuccessErrorModal';
import useTasker from '../../../hooks/useTasker';
import useUser from '../../../hooks/useUser';
import { setUser } from '../../../store/reducers/user/user.actions';
import { TaskerForm } from './DashboardTaskerForm.style';

const formStyle = {
  display: 'flex',
  flexDirection: 'column',
};

const DashboardTaskerForm = () => {
  const [formData, setFormData] = useState(initValues);
  const { user } = useSelector((state) => state.currentUser);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const { setIsTaskerData } = useUser();
  const { postTasker } = useTasker();
  const [openSuccessErrorModal, setSuccessErrorModal] = useState(false);
  const [isSuccess, setSuccess] = useState(false);
  const [response, setResponse] = useState('');
  const handleSubmit = async (e) => {
    e.preventDefault();
    if (Object.values(formData).includes('')) {
      alert('Some parts are still empty');
      return;
    }

    const res = await postTasker(user.id, formData);
    SuccessErrorPopout(res === 'success', res);
  };

  const SuccessErrorPopout = (isPosted, res) => {
    if (isPosted) {
      setIsTaskerData(user.id);
      dispatch(setUser({ ...user, is_tasker_data: true }));
      navigate('/dashboard', { replace: true });
      setResponse('Congragulation! You are a tasker now!');
    } else {
      setResponse(res);
    }
    setSuccess(isPosted);
    setSuccessErrorModal(true);
  };

  return (
    <>
      <TaskerForm style={formStyle} noValidate autoComplete="off" onSubmit={handleSubmit}>
        <TaskerInput title="Title" name="title" formData={formData} setFormData={setFormData} />
        <CategorySelection formData={formData} setFormData={setFormData} />
        <TaskerInput title="Skills" name="skills_description" formData={formData} setFormData={setFormData} />
        <TaskerInput title="Certifications" name="certifications" formData={formData} setFormData={setFormData} />

        <Box
          sx={{
            display: 'flex',
            width: '70%',
          }}
        >
          <PaymentInput formData={formData} setFormData={setFormData} />
        </Box>
        <div
          className="submit"
          style={{ width: '92%', display: 'flex', alignItems: 'center', justifyContent: 'end', margin: '1rem' }}
        >
          <CustomButton color="primary" variant="contained" type="submit">
            Submit
          </CustomButton>
        </div>
      </TaskerForm>
      <SuccessErrorModal
        isSuccess={isSuccess}
        setShowModal={setSuccessErrorModal}
        showModal={openSuccessErrorModal}
        response={response}
      />
    </>
  );
};

export default DashboardTaskerForm;
