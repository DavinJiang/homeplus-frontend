import styled from 'styled-components';

export const MenuBox = styled.div`
  margin: 0;
  min-height: 88vh;
  position: relative;
  left: -20px;
  display: flex;
  border-right: 1.3px solid #f2f2f2;
  background-color: #fff;
  z-index: 99;
  .expandButton {
    display: none;
    height: 20px;
  }
  @media (max-width: 1152px) {
    position: fixed;
    padding-top: 30px;
    min-height: 100%;

    min-width: ${({ isMenu }) => (isMenu ? 'none' : '0px')};
    nav {
      display: ${({ isMenu }) => (isMenu ? 'inline' : 'none')};
    }
    .expandButton {
      width: 27px;
      height: 27px;
      background-color: #afafaf;
      display: flex;
      transform: ${({ isMenu }) => (isMenu ? 'rotate(180deg)' : 'rotate(0deg)')};
      align-items: center;
      justify-content: center;
      position: absolute;
      border-radius: 50%;
      left: ${({ isMenu }) => (isMenu ? '280px' : '30px')};
      top: 85px;
      z-index: 999;
    }
  }
`;
