import styled from 'styled-components';

export const InfoCard = styled.div`
  h3,
  span {
    color: #444;
  }
  .MuiAvatar-root {
    margin-right: 13%;
    position: relative;
    top: 5px;
  }
  svg {
    position: relative;
    left: 14%;
    width: 30px;
    transform: translateX(-50%);
    transform: scale(1.2);
  }
  display: flex;
  align-items: center;

  .info {
    min-width: 100px;
    span {
      font-size: 0.95rem;
      width: 60%;
    }
    .numbers {
      font-size: 1.5rem;
      font-weight: 500;
    }
    h3 {
      font-size: 1rem;
    }
  }

  @media (max-width: 768px) {
    .info {
      h3 {
        font-size: 16px;
      }
      span {
        font-size: 14px;
      }
      .numbers {
        font-size: 1.5rem;
        font-weight: 400;
      }
    }
    .MuiAvatar-root {
      margin-right: 15px;
    }
  }
`;
