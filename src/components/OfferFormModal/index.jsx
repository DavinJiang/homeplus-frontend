import React, { useState } from 'react';
import _ from 'lodash';
import { useParams } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import Input from '@mui/material/Input';
import Modal from '@mui/material/Modal';
import InputAdornment from '@mui/material/InputAdornment';
import TextField from '@mui/material/TextField';
import SuccessErrorModal from '../SuccessErrorModal';
import CustomButton from '../CustomButton';
import useOffer from '../../hooks/useOffer';
import { Form, OfferFormBox } from './OfferFormModal.styled';
import { askLogin } from '../../store/reducers/user/user.actions';

const OfferFormModal = ({ showModal, setShowModal, offersRequest }) => {
  const currentUser = useSelector(state => state.currentUser);
  const dispatch = useDispatch(); 
  const { postOffer } = useOffer();
  const { task_id } = useParams();
  const [ openSuccessErrorModal, setSuccessErrorModal ] = useState(false);
  const [ isSuccess, setSuccess ] = useState(false);
  const [ response, setResponse] = useState('');

  const handleClose = () => setShowModal(false);

  const [isError, setError] = useState(false);

  const [offerInfo, setOfferInfo] = useState({
    task_id: task_id,
    offered_price: 0,
    description: ""
  });

  const handleSubmit = async (e) => {
    e.preventDefault();
    setError(false);
    
    if (!offerInfo.offered_price) {
      setError(true);
      return;
    }

    if (!currentUser.loggedIn) {
      setShowModal(false);
      dispatch(askLogin());
      return;
    };

    if (_.isEmpty(currentUser.tasker)) {
      alert('Become a tasker on Dashboard');
      return;
    }
    const offerToSend = { ...offerInfo, tasker_id: currentUser.tasker.id }
    const res = await postOffer(offerToSend);
    offersRequest(task_id);
    setShowModal(false);
    SuccessErrorPopout(res === 'success', res);
    resetOfferInfo();    
  }

  const SuccessErrorPopout = (isPosted, res) => {
    if(isPosted) {
      setResponse('Your offer sent successfully');
    } else {
      setResponse(res);
    }
    setSuccess(isPosted);
    setSuccessErrorModal(true);
  }

  const resetOfferInfo = () => {
    setOfferInfo({
      ...offerInfo,
      offered_price: 0,
      description: ""
    })
  }

  const handleChange = (e) => {
    e.preventDefault();
    setOfferInfo({ ...offerInfo, [e.target.name]: e.target.value });
  };

  return (
    <>
       <Modal
        open={showModal}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <OfferFormBox>
          <Form onSubmit={handleSubmit}>
            <FormControl sx={{ m: 1}}>
              <InputLabel htmlFor="offered_price">My Offer</InputLabel>
              <Input
                id="offered_price"
                name="offered_price"
                value={offerInfo.offered_price}
                type='number'
                onChange={(e) => setOfferInfo({ ...offerInfo, [e.target.name]: (e.target.value < 0 ? 0 :  e.target.value)})}
                variant="outlined"
                style={{ backgroundColor: '#E5E5E5' }}
                startAdornment={<InputAdornment position="start">$</InputAdornment>}
                error={isError}
              />
            </FormControl>
            <FormControl sx={{ m: 1}}>
              <TextField
                id="description"
                onChange={handleChange}
                value={offerInfo.description}
                name="description"
                label="Message to client"
                multiline
                rows={4}
                fullWidth
              />
            </FormControl>
            <CustomButton variant="contained" type="submit">
              Submit
            </CustomButton>
          </Form>
        </OfferFormBox>
      </Modal>
      <SuccessErrorModal isSuccess={isSuccess} setShowModal={setSuccessErrorModal} showModal={openSuccessErrorModal} response={response}/>
    </>
  );
};

OfferFormModal.propTypes = {
  showModal: PropTypes.bool,
  setShowModal: PropTypes.func,
  offersRequest: PropTypes.func,
};

export default OfferFormModal;
