import styled from 'styled-components';

export const Message = styled.div`
  //   height: 92vh;
  width: 100%;
  background: #fff;
  padding-top: 10%;
  font-size: 1.2rem;
  display: flex;
  flex-direction: column;
  align-items: center;
  > p {
    text-align: center;
  }
`;
