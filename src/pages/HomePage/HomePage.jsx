import React, { useState, useEffect } from 'react';
import HomePageInfo from './HomePageInfo';
import HomePageCategories from './HomePageCategories/index';
import HomePageTaskStep from './HomePageTaskStep';
import HomePageCard from './HomePageInstruction';
import Footer from '../../components/Footer';
import { Wrapper } from './HomePage.style';

const HomePage = () => {
  const [scrollPosition, setScrollPosition] = useState(0);
  const handleScroll = () => {
    const position = window.pageYOffset;
    setScrollPosition(position);
  };

  useEffect(() => {
    window.addEventListener('scroll', handleScroll);

    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, []);

  return (
    <Wrapper>
      <HomePageInfo />
      <HomePageCategories />
      <HomePageTaskStep scrollPosition={scrollPosition} />
      <HomePageCard />
      <Footer />
    </Wrapper>
  );
};

export default HomePage;
